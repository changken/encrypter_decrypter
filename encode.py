# -*- coding: utf-8 -*-
import rsa, os

bufferSize = 64 * 1024

def generate_key(pubkey_name='public.pem', prikey_name='private.pem'):
    # output a pair of rsa key
    (publickey, privatekey) = rsa.newkeys(2048)

    pub = publickey.save_pkcs1() # save public key
    pri = privatekey.save_pkcs1() # save private key

    # output rsa key as a file
    with open(pubkey_name, 'wb') as f:
        f.write(pub)
    
    with open(prikey_name, 'wb') as f:
        f.write(pri)
    
    return publickey, privatekey

def load_pubkey(pubkey_path='public.pem'):
    if os.path.isfile(pubkey_path):
        # load public key
        with open(pubkey_path, 'rb') as f:
            pub = f.read()
            pub = rsa.PublicKey.load_pkcs1(pub)
    else:
        # generate a new one
        pub, pri = generate_key(pubkey_name=pubkey_path)
        
    return pub

def load_prikey(prikey_path='private.pem'):
    if os.path.isfile(prikey_path): 
        # load private key
        with open(prikey_path, 'rb') as f:
            pri = f.read()
            pri = rsa.PrivateKey.load_pkcs1(pri)
        
        return pri
    else:
        raise Exception("No private key!")
        
def generate_aes_key(aeskey_path='aeskey_encoded.txt'):
    # generate aeskey
    aeskey = rsa.randnum.read_random_bits(128)
    
    # encode aes key and save a file
    pub = load_pubkey()
    with open(aeskey_path, 'wb') as f:
        f.write(rsa.encrypt(aeskey, pub))
    
    # return non-encoded one
    return str(aeskey)

def load_aes_key(aeskey_path='aeskey_encoded.txt'):
    # check encoded aes file
    if os.path.isfile(aeskey_path):
        with open(aeskey_path, 'rb') as f:
            pri = load_prikey()
            # decrypt encoded aes password
            aeskey = rsa.decrypt(f.read(), pri)
        # get non-encoded one
        return str(aeskey)
    else:
        return generate_aes_key(aeskey_path=aeskey_path)
    

if __name__ == "__main__":
    # pub = load_pubkey()
    
    # print(pub)
    #print(pub.decode('utf8'))
    
    aeskey = load_aes_key()
    print(aeskey)