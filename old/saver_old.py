# -*- coding: utf-8 -*-
import base64, glob, re, pyAesCrypt, encode, os

pattern = re.compile('^(.*).(doc|docx|xls|xlsx|ppt|pptx)_encoded.aes$')

# load aes key
aeskey = encode.load_aes_key()

lst_this_folder = glob.glob('*')
for fname in lst_this_folder:
    if pattern.match(fname):
        # find decoded file name
        decoded_fname = fname.split('_')[0]
        # find base64 file name
        base64_fname = os.path.splitext(fname)[0]
        # if decoded file is not exist
        if decoded_fname not in lst_this_folder:
            # use aes key to decode
            pyAesCrypt.decryptFile(fname, base64_fname, aeskey, encode.bufferSize)            
            # get file name and content
            with open(base64_fname, 'rb') as f:
                # use base64 to decode
                decoded_content = base64.b64decode(f.read())
            # decode file
            with open(decoded_fname, 'wb') as decoded_f:
                decoded_f.write(decoded_content)
            print('開勳! 救到%s了!'%(decoded_fname))
            os.remove(fname)
            os.remove(base64_fname)
        else:
            print('不需要救!')
input("Please enter to continue!")